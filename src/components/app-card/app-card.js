import './app-card.css';


export const AppCard = (props) => {

    const appCard  = document.createElement('div');
    appCard.classList.add('card');

    appCard.innerHTML = `
    <img class="healthcare__card_icon" src="${props.image}" alt=""/>
    <h4 class="healthcare__card_title">${props.title}</h4>
    <p class="healthcare__card_text">${props.text}</p>
    `;

    return appCard;
};
