import "./app-heading.css";

export function AppHeading(props) {
    const heading = document.createElement('h2');

    const l = props.text.indexOf(props.coloredText);
    const r = l + props.coloredText.length;
    // console.log(props.text.slice(0, l));
    // console.log(props.text.slice(l, r));
    // console.log(props.text.slice(r, props.text.length));

    const leftText = document.createElement('span');
    leftText.innerText = props.text.slice(0, l);
    const middleText = document.createElement('span');
    middleText.innerText = props.text.slice(l, r);
    middleText.style.color = props.color;
    const rightText = document.createElement('span');
    rightText.innerText = props.text.slice(r, props.text.length);

    heading.append(leftText);
    heading.append(middleText);
    heading.append(rightText);

    return heading;
  }