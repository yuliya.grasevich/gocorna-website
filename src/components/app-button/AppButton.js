import "./app-button.css";

export function AppButton(props) {
    const button = document.createElement("button");
  
    button.innerText = props.label; 

    if (props.type === 'primary') {
        button.style.backgroundColor = "#EC5863";
    }else{
        button.style.backgroundColor = "#4285F4";
    }

    button.style.color = "white";
  
    return button;
  }
