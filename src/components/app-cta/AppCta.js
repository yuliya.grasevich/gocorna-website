import "./cta.css";
import bgImage from "../../assets/Group 28.png";

export function AppCta() {
    const cta = document.createElement('div');

    cta.classList.add('cta');

    cta.innerHTML = `
    <div class="cta__bg"><img src="${bgImage}" alt=""></div>
    <div class="cta__content">
       <div class="tr">
          <span class="th">2m</span>
          <span class="td">Users</span>
       </div>
       <div class="tr">
          <span class="th">78</span>
          <span class="td">Countries</span>
       </div>
       <div class="tr">
          <span class="th">10,000+</span>
          <span class="td">Medical experts</span>
       </div>
    </div>
    `;

    return cta;
}