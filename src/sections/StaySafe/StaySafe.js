import "./stay-safe.css";
import heroImage from '/src/assets/Group 22.png'
import {AppButton} from "../../components/app-button/AppButton";
import {AppHeading} from "../../components/app-heading/AppHeading";

export function StaySafe() {
    const section = document.createElement('section');
    const button = AppButton({
        label: 'Features',
        type: 'primary'
    })
    const title = AppHeading({
        text: "Stay safe with GoCorona.",
        color: "#EC5863",
        coloredText: 'Corona.'
    })

    section.classList.add('section-safe');

    section.innerHTML = `
        <div class="container">
              <div class="content-split">
                  <div class="content-split__image">
                    <img src="${heroImage}" alt="">
                  </div>
              <div class="content-split__content">
                  <p class="safe-text">24x7 Support and user friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.</p>
               </div>
               </div>
        </div>
    `;

    section.querySelector('.safe-text').before(title);
    section.querySelector('.safe-text').after(button);
    return section;
}