import './healthcare.css';
import { AppHeading } from "../../components/app-heading/AppHeading";
import { AppCard } from "../../components/app-card/app-card";
import manIcon from "../../assets/group42.svg";
import doctorIcon from "../../assets/group43.svg";
import heartIcon from "../../assets/group45.svg";
import googlePlayIcon from "../../assets/googleplay.png";
import appleStoreIcon from "../../assets/appstore.png";


export const Healthcare = () => {

    const healthcare  = document.createElement('section');
    healthcare.classList.add('healthcare');

    const title = AppHeading({
        text: "Healthcare at your Fingertips.",
        color: "red",
        coloredText: 'Healthcare'
    })

    const description  = document.createElement('p');
    description.classList.add('healthcare__description');
    description.innerText = `Bringing premium healthcare features to your fingertips. User-friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.`;

    const cta  = document.createElement('div');
    cta.classList.add('healthcare__cta');

    const ctaValue = [
        {
            image: manIcon,
            title: 'Symptom Checker',
            text: 'Check if you are infected by COVID-19 with our Symptom Checker'
        },
        {
            image: doctorIcon,
            title: '24x7 Medical support',
            text: 'Consult with 10,000+ health workers about your concerns.'
        },
        {
            image: heartIcon,
            title: 'Conditions',
            text: 'Bringing premium healthcare features to your fingertips.'
        }
    ];

    ctaValue.forEach((product) => {
        const card = AppCard(product);
        cta.append(card);
    });

    const links  = document.createElement('div');
    links.classList.add('healthcare__links');
    links.innerHTML = `
    <img class="healthcare__links_icon" src="${googlePlayIcon}"  alt="googlePlay icon"/>
    <img class="healthcare__links_icon" src="${appleStoreIcon}"  alt="appleStore icon"/>
    `;

    healthcare.append(title);
    healthcare.append(description);
    healthcare.append(cta);
    healthcare.append(links);

    return healthcare;
};