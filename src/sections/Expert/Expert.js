import sectionImage from "../../assets/Group 15.png";
import "./expert.css";
import {AppHeading} from "../../components/app-heading/AppHeading";
import {AppButton} from "../../components/app-button/AppButton";
import {AppCta} from "../../components/app-cta/AppCta";

export function Expert() {
    const section = document.createElement('section');

    const button = AppButton({
        label: 'Features',
        type: 'primary'
    })

    const cta = AppCta();

    const title = AppHeading({
        text: "Talk to experts.",
        color: "#587EEC",
        coloredText: 'experts.'
    })

    section.classList.add('section-expert');

    section.innerHTML = `
        <div class="container">
           <div class="content-split content-split--safe">
              <div class="content-split__content">
                  <p class="safe-text">Book appointments or submit queries into thousands of forums concerning health issues and prevention against noval Corona Virus.</p>
              </div>
              <div class="content-split__image">
                <div class="image-box">
                     <img src="${sectionImage}" alt="">
                </div>
              </div>
           </div>
        </div>
    `;

    section.querySelector('.safe-text').before(title);
    section.querySelector('.content-split').before(cta);
    section.querySelector('.safe-text').after(button);
    return section;
}