import "./hero.css";
import heroImage from '/src/assets/Group 14.png'
import coronaImage from '/src/assets/Group2.svg'
import playImage from '/src/assets/Group 23.svg'
import { AppButton } from "../../components/app-button/AppButton";
import { AppHeading } from "../../components/app-heading/AppHeading";

export function Hero(){
    const section = document.createElement('section');
    const getStartedButton = AppButton({
        label: 'Get Started',
        type: 'primary'
    })
    const downloadButton = AppButton({
        label: 'Download',
        type: 'secondary'
    })

    const title = AppHeading({
        text: "Take care of yours family’s health.",
        color: "blue",
        coloredText: 'health.'
    })

    section.classList.add('section-hero');

    section.innerHTML = `
        <div class="hero">
            <div class="header">
                <img class="header__logo" src="${coronaImage}" alt="gocorona">
                <nav class="header__links">
                    <a href="#" class="header__link">HOME</a>
                    <a href="#" class="header__link">FEATURES</a>
                    <a href="#" class="header__link">SUPPORT</a>
                    <a href="#" class="header__link">CONTACT US</a>
                </nav>
            </div>
            <div class="intro">
                <div class="intro__left">
                    <div class="intro__message">
                        <p class="safe-text">All in one destination for COVID-19 health queries.<br>Consult 10,000+ health workers about your conserns.</p>
                    </div>
                </div>
                <div class="intro__right">
                    <img class="intro__image" src="${heroImage}" alt="">
                </div>
            </div>
            <div class="play">
                <img src="${playImage}" alt="play">
                <div class="play__content">
                    <p class="play__header">Stay safe with GoCorona</p>
                    <p class="play__watch">Watch Video</p>
                </div>
            </div>
        </div>
    `;

    section.querySelector('.header').appendChild(downloadButton);
    section.querySelector('.safe-text').before(title);
    section.querySelector('.safe-text').after(getStartedButton);

    // const container = section.querySelector('.hero');
    // container.style.backgroundImage = `url(${backgroundImage})`;
    // console.log(container);

    return section
}