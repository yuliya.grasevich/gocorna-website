import "./reset.css";
import "./styles.css";
import { Hero } from "./sections/Hero/Hero";
import { StaySafe } from "./sections/StaySafe/StaySafe";
import { Expert } from "./sections/Expert/Expert";
import { Healthcare } from "./sections/Healthcare/Healthcare";

const app = document.querySelector("#app"); // div#app

app.append(Hero());

app.append(StaySafe());

app.append(Expert());

app.append(Healthcare());
